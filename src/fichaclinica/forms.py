from django import forms
from .models import Ficha

class RegModelForm(forms.ModelForm):
	class Meta:
		model = Ficha
		campos = ["enfermedad_detectada", "tratamiento", "fecha_enfermedad", "fecha_termino", "resultado"]
		exclude = ()

		#validaciones
	def clean_tratamiento(self):
		tratamiento=self.cleaned_data.get("tratamiento")
		if tratamiento:
			if len(tratamiento) >= 5 and len(tratamiento) <= 80:
				return tratamiento
			else:
				raise forms.ValidationError("Numero de caracteres insuficientes")