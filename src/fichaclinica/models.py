from django.db import models

# Create your models here.

class Ficha(models.Model):
	id_ficha = models.AutoField(primary_key=True, unique=True)
	enfermedad_detectada = models.CharField(max_length=100)
	tratamiento = models.CharField(max_length=100)
	fecha_enfermedad = models.DateField()
	fecha_termino = models.DateField()
	resultado = models.CharField(max_length=100)

	def __str__(self):
		return self.enfermedad_detectada