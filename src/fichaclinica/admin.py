from django.contrib import admin
from .models import *
from .forms import RegModelForm
# Register your models here.

class AdminFicha (admin.ModelAdmin):
	list_display = ["enfermedad_detectada", "tratamiento", "fecha_enfermedad", "fecha_termino", "resultado"]
	form = RegModelForm
	list_filter = ["enfermedad_detectada", "fecha_enfermedad"]
	search_fields = ["fecha_enfermedad"]

admin.site.register(Ficha, AdminFicha)