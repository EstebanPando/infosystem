from django.shortcuts import render
from .forms import RegModelForm
from .models import Ficha
# Create your views here.

def datosFicha(request):
	form1 = RegModelForm(request.POST or None)
	if request.user.is_authenticated():
		titulo1 = "Bienvenido a La Aplicacion Web %s" %(request.user)

		contexto = {
			"formulario_ficha":form1,
			"title": titulo1,
		}
	if form1.is_valid():
		instance = form1.save(commit=False)
		instance.save()
		print(instance)
	return render(request, 'ficha.html', contexto)