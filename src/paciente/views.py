from django.shortcuts import render
from .forms import RegModelForm, ContactForm
from django.conf import settings
from django.core.mail import send_mail
# Create your views here.

def datosPaciente(request):
	form = RegModelForm(request.POST or None)
	if request.user.is_authenticated():
		titulo = "Bienvenido a La Aplicacion Web %s" %(request.user)
		mensaje = "datos guardados exitosamente"
	contexto = {
		"formulario_paciente":form,
		"title": titulo,
	}

	if form.is_valid():
		instance = form.save(commit=False)
		if not instance.nombre:
			instance.nombre = "SIN NOMBRE"
		instance.save()
		contexto = {
			"mensaje":mensaje,
		}
		print(instance)
	return render(request, 'patient.html', contexto)

def contacto(request):
	form = ContactForm(request.POST or None)
	if form.is_valid():
		#print (form.cleaned_data)

		formulario_nombre = form.cleaned_data.get("nombre")
		formulario_email = form.cleaned_data.get("email")
		formulario_mensaje = form.cleaned_data.get("mensaje")
		asunto = "Formulario de contacto web"
		email_from = settings.EMAIL_HOST_USER
		email_to = [email_from, "jorgeborquez@udec.cl"]
		email_mensaje = "Enviado por %s  - Correo: %s  - Mensaje: %s" %(formulario_nombre, formulario_email, formulario_mensaje)
		send_mail(asunto, email_mensaje, email_from, email_to, fail_silently=False)

	contexto = {
		"el_contacto":form,
	}
	return render(request, "contacto.html", contexto)