from django import forms
from .models import Patient

class RegModelForm(forms.ModelForm):
	class Meta:
		model = Patient
		campos = ["nombre", "rut", "edad_ingreso", "edad_actual", "fecha_ingreso"]
		exclude = ()

	def clean_edad_ingreso(self):
		edad_ingreso = self.cleaned_data.get("edad_ingreso")
		if edad_ingreso:
			if edad_ingreso <= 0:
				raise forms.ValidationError("No se puede ingresar valores en 0")
			if edad_ingreso < 100:
				return edad_ingreso
			else:
				raise forms.ValidationError("no se aceptan valores sobre 100")

class ContactForm(forms.Form):
	nombre = forms.CharField(required=False)
	email = forms.EmailField()
	mensaje = forms.CharField(widget=forms.Textarea)
