from django.contrib import admin
from .models import *
from .forms import RegModelForm
# Register your models here.

class adminPatient(admin.ModelAdmin):
	list_display = ["nombre", "rut", "edad_ingreso"]
	form = RegModelForm
	list_filter = ["nombre", "rut"]
	search_fields = ["fecha_ingreso"]

admin.site.register(Patient, adminPatient)