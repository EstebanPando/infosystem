from django.db import models

# Create your models here.

class Patient(models.Model):
	id_paciente = models.AutoField(primary_key=True, unique=True)
	nombre = models.CharField(max_length=100, blank=True, null=True)
	rut = models.IntegerField(max_length=12, blank=False, null=False)
	edad_ingreso = models.IntegerField()
	edad_actual = models.IntegerField()
	fecha_ingreso = models.DateField()
	#ficha_clinica = models.ForeignKey('fichaclinica.Ficha')
	def __str__(self):
		return self.nombre